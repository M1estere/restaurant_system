﻿using System;

namespace Second_Practice;

internal class ClientAccount : Account, IObserve
{
    public ClientAccount(AccountType accountType, int id) : base(accountType, id) {  }

    public void GetNotified()
    {
        Console.WriteLine("ClientAccount: Notify() : Your order made in app is ready");
    }
}
