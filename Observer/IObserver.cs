﻿namespace Second_Practice;

internal interface IObserver
{
    void Notify();
}
