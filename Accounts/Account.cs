﻿namespace Second_Practice;

internal abstract class Account
{
    internal AccountType AccountType { get; private set; }

    internal int Id { get; private set; }

    private string _name;
    private string _email;

    protected Account(AccountType accountType, int id)
    {
        AccountType = accountType;
        Id = id;
    }

    public override string ToString() => $"{AccountType} account:\n\tName: {_name}\n\tEmail: {_email}";
}
