﻿using System;

namespace Second_Practice;

internal class Order : IObserver
{
    internal int OrderId { get; private set; }
    internal OrderState OrderState { get; set; }

    private IObserve currentObserve;

    private DateTime _orderTime;

    private int _price;

    public Order(IObserve observe, int id)
    {
        OrderId = id;

        OrderState = OrderState.Processed;

        currentObserve = observe;
    }

    internal void ChangeState(OrderState newState)
    {
        OrderState = newState;

        if (newState == OrderState.Ready) Notify();
    }

    public void Notify() => currentObserve.GetNotified();
}
