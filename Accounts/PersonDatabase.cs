﻿using System.Collections.Generic;

namespace Second_Practice;

internal static class PersonDatabase
{
    private static List<Account> _accounts = new ();

    internal static int AccountCounter { get; private set; } = 0;

    internal static Account AddNewCorporateAccount(AccountType accountType)
    {
        CorporateAccount result = new CorporateAccount(accountType, AccountCounter);

        _accounts.Add(result);
        AccountCounter++;

        return result;
    }

    internal static Account AddNewClientAccount(AccountType accountType)
    {
        ClientAccount result = new ClientAccount(accountType, AccountCounter);

        _accounts.Add(result);
        AccountCounter++;

        return result;
    }

    internal static Account Authorization(int accountId)
    {
        foreach (Account account in _accounts)
        {
            if (account.Id != accountId) continue;

            return account;
        }

        return null;
    }
}
