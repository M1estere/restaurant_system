﻿using System;

namespace Second_Practice;

internal class Pager : IObserve
{
    public void GetNotified()
    {
        Console.WriteLine("Pager: Notify() : Your order made in live is ready");
    }
}
