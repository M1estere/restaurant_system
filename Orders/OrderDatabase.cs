﻿using System.Collections.Generic;

namespace Second_Practice;

internal static class OrderDatabase
{
    private static List<Order> _notReadyOrders = new ();
    private static List<Order> _givenOrders = new ();

    internal static int OrderCount { get; private set; } = 0;

    internal static void CreateNewOrderByClient(IObserve phoneApplication)
    {
        _notReadyOrders.Add(new Order(phoneApplication, OrderCount));
        OrderCount++;
    }

    internal static void CreateNewOrderByCashier()
    {
        _notReadyOrders.Add(new Order(new Pager(), OrderCount));
        OrderCount++;
    }

    internal static void ChangeOrderState(int orderId, OrderState newState)
    {
        Order orderGlob = null;
        foreach (Order order in _notReadyOrders)
        {
            if (order.OrderId != orderId) continue;

            order.ChangeState(newState);

            orderGlob = order;
        }

        if (newState == OrderState.Ready)
        {
            _givenOrders.Add(orderGlob);
            _notReadyOrders.Remove(orderGlob);
        }
    }
}
