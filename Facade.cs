﻿using System;

namespace Second_Practice;

internal static class Facade
{
    internal static Account Authorization(int id) => PersonDatabase.Authorization(id);

    internal static Account CreateCorporateAccount(Account requestAccount)
    {
        if (requestAccount.AccountType is AccountType.Corporate)
        {
            Console.WriteLine("Facade: CreateCorporateAccount(): Successfully created corporate account");
            return PersonDatabase.AddNewCorporateAccount(AccountType.Corporate);
        } else
        {
            Console.WriteLine("Facade: CreateCorporateAccount(): Failed to create corporate account (wrong account type)");
        }

        return null;
    }

    internal static Account CreateClientAccount() => PersonDatabase.AddNewClientAccount(AccountType.Client);
    
    internal static void CreateNewOrderByCashier(Account requestAccount)
    {
        if (requestAccount.AccountType is AccountType.Corporate)
        {
            OrderDatabase.CreateNewOrderByCashier();
            Console.WriteLine("Facade: CreateNewOrderByCashier(): Successfully created order by cashier");
        } else
        {
            Console.WriteLine("Facade: CreateNewOrderByCashier(): Failed to create order by cashier (wrong account type)");
        }
    }

    internal static void CreateNewOrderByClient(Account requestAccount)
    {
        if (requestAccount.AccountType is AccountType.Client)
        {
            if (requestAccount is ClientAccount)
                OrderDatabase.CreateNewOrderByClient(requestAccount as ClientAccount);
            Console.WriteLine("Facade: CreateNewOrderByClient(): Successfully created new order by client");
        } else
        {
            Console.WriteLine("Facade: CreateNewOrderByClient(): Failed to create new order by client (wrong account type)");
        }
    }

    internal static void ChangeOrderState(Account requestAccount, int orderId, OrderState newState)
    {
        if (requestAccount.AccountType is AccountType.Corporate)
        {
            OrderDatabase.ChangeOrderState(orderId, newState);
            Console.WriteLine("Facade: ChangeOrderState(): Successfully changed state of order to " + newState);
        } else
        {
            Console.WriteLine("Facade: ChangeOrderState(): Failed to change state of order (wrong account type)");
        }
    }
}
