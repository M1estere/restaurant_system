# Информационная система ресторана

## Наблюдатель
Паттерн наблюдатель — это паттерн проектирование, в котором объект, ведет список своих зависимых объектов, называемых наблюдателями, и автоматически уведомляет их о любых изменениях состояния, обычно вызывая один из их методов.

## Фасад
Фасад - это паттерн проектирования программного обеспечения, который позволяет упростить интерфейс для более сложной системы или подсистемы. Он обеспечивает унифицированный интерфейс для набора интерфейсов в подсистеме. Фасад определяет интерфейс более высокого уровня, который упрощает использование подсистемы, предоставляя единую точку входа для доступа к функциям базовой системы.

## Функции
Система имеет следующий функционал:
- создание корпоративного или клиентского аккаунта
- создание онлайн заказа или заказа в живую
- изменение состояния заказа
- оповещение клиента о готовности заказа к выдаче

В качестве фасада в системе выступает класс Facade, он служит для работы по созданию аккаунтов, заказов, а также изменения состояния заказа путем обращения к другим классам.

В качестве наблюдателя интерфейсы IObserve и IObserver. От IObserve наследуются классы ClientAccount и Pager. От IObserver наследуется класс Order для того, чтобы при изменении статуса заказа клиента на "Ready" он оповещал IObserve. 
