﻿using System;

namespace Second_Practice;

internal class Program
{
    private static void Main(string[] args)
    {
        Account account;
        account = new CorporateAccount(AccountType.Corporate, -1);


        account = Facade.CreateCorporateAccount(account);

        Facade.CreateNewOrderByCashier(account);

        Facade.ChangeOrderState(account, 0, OrderState.Ready);

        account = Facade.CreateClientAccount();

        Facade.CreateCorporateAccount(account); // error line

        Facade.CreateNewOrderByCashier(account); // error line

        Facade.CreateNewOrderByClient(account);

        account = Facade.Authorization(0);

        Facade.ChangeOrderState(account, 1, OrderState.Ready);


        Console.ReadKey();
    }
}
