﻿namespace Second_Practice;

internal enum OrderState
{
    Processed, Accepted, Cooking, Ready, Given,
}
