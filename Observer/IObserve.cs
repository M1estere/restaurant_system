﻿namespace Second_Practice;

internal interface IObserve
{
    void GetNotified();
}
